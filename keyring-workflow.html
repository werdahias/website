<!doctype HTML public "-//W3C//DTD HTML 3.2//EN">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="https://www.debian.org/favicon.ico" rel="shortcut icon" />
	<title>keyring.debian.org</title>

	<link rel="stylesheet" href="https://www.debian.org/debian.css" type="text/css" />
</head>
<body>

<div id="header">

        <div id="upperheader">
                <div id="logo">
                        <a href="https://www.debian.org/" title="Debian Home"><img src="/Pics/openlogo-50.png" alt="Debian" width="50" height="61" /></a>
                </div>
                <p class="section">keyring</p>
		
        </div>

        <div id="navbar">

                <p class="hidecss"><a href="#inner">Skip Quicknav</a></p>
                <ul>
                   <li><a href="https://www.debian.org/intro/about">About Debian</a></li>
                   <li><a href="https://www.debian.org/distrib/">Getting Debian</a></li>
                   <li><a href="https://www.debian.org/support">Support</a></li>
                   <li><a href="https://www.debian.org/devel/">Developers'&nbsp;Corner</a></li>

                </ul>
        </div>
	<p id="breadcrumbs"><a href="./">keyring.debian.org</a> / Keyring Workflow</p>
</div> 


<div id="content">

<p>This page attempts to describe the various public interfaces to the
Debian keyring, and explain the ways in which they differ. It is
important to realise that they are not all equal in order to understand
why a submitted keyring update may not yet be visible to the Debian
infrastructure. This may be the case even though keyring-maint has said
the update has been performed, or it is visible via some of the public
interfaces already. Equally a key update may be visible to the Debian
infrastructure despite not being updated somewhere, for example in the
<tt>debian-keyring</tt> package in the archive.</p>

<p>First, a diagram to show the different interfaces to the keyring and
how they connect to each other:</p>

<p><img src="/Pics/key-flow.png" alt="keyring-maint workflow" /></p>

<h2>Public interfaces</h2>

<h3>rsync: keyring.debian.org::keyrings</h3>

<p>This is the most important public interface; it's the one that the
Debian infrastructure uses. This is
where <a href="https://ftp-master.debian.org/">ftp-master</a> gets its
information shortly after it is updated, for example. It's the
canonical location of the active set of Debian keyrings and is what
you should be using if you want the most up to date copy. The validity
of the keyrings can be checked using the
included <code>sha512sums.txt</code> file, which will be signed by
whoever in keyring-maint did the last keyring update.</p>

<h3>HKP interface: hkp://keyring.debian.org/</h3>

<p>What you talk to with <code>gpg --keyserver
keyring.debian.org</code>. Serves out the current keyrings, and accepts
updates to any key it already knows about (allowing, for example, expiry
updates, new subkeys + uids or new signatures without the need to file a
ticket in RT or otherwise explicitly request it). Updates sent to this
interface will be available via it within a few hours, but must be
manually folded into the active keyring. This in general happens about
once a month when preparing for a general update of the keyring; for
example <a href="https://salsa.debian.org/debian-keyring/keyring/commit/b490c1d5f075951e80b22641b2a133c725adaab8">b490c1d5f075951e80b22641b2a133c725adaab8</a>.</p>

<p>Why not do this automatically? Even though the site uses GnuPG to verify
incoming updates there are still occasions were bugs have been seen (such as <a
href="https://bugs.debian.org/787046">#787046</a>, where GnuPG would always
import subkeys it didn't understand, even when that subkey was already
present). Also it is not desirable to allow just any UID to be part of the
keyring. It is thus useful to retain a final set of human based sanity checking
for any update before it becomes part of the keyring proper.</p>

<h3>Salsa: <a href="https://salsa.debian.org/debian-keyring/keyring/">https://salsa.debian.org/debian-keyring/keyring/</a></h3>

<p>A public mirror of the git repository the keyring-maint team use to
maintain the keyring. Every action is recorded here, and in general each
commit should be a single action (such as adding a new key, doing a key
replacement or moving a key between keyrings). Note that pulling in the
updates sent via HKP count as a single action, rather than having a
commit per key updated. This mirror is updated whenever a new keyring is
made active (i.e. made available via the rsync interface). Until that
point pending changes are kept private; keyring-maint sometimes deals
with information such as the fact someone has potentially had a key
compromised that shouldn't be public until it has actually been
disabled. Every “keyring push” (as the process of making a new keyring
active is referred to) is tagged with the date it was performed.
Releases are also tagged with their codenames, to make it easy to do
comparisons over time.</p>

<h3><a href="https://tracker.debian.org/pkg/debian-keyring">Debian archive</a></h3>

<p>This is actually the least important public interface to the keyring,
at least from the perspective of the keyring-maint team. No
infrastructure makes use of it and while it's mostly updated when a new
keyring is made active a concerted effort to do so is only made when it
is coming up to release. It's provided as a convenience package rather
than something which should be utilised for active verification of which
keys are and aren't currently part of the keyring.</p>

<h2>Team interface</h2>

<h3>Master repository: kaufmann.debian.org:/srv/keyring.debian.org/master-keyring.git</h3>

<p>The master git repository for keyring maintenance is stored on
kaufmann.debian.org AKA keyring.debian.org. This system is centrally
managed by <a href="https://dsa.debian.org/">DSA</a>, with only DSA and
keyring-maint having login rights to it. None of the actual maintenance
work takes place here; it is a bare repo providing a central point for
the members of keyring-maint to collaborate around.</p>

<h2>Private interface</h2>

<h3>Private working clone</h3>

<p>This is where all of the actual keyring work happens. Team members
have a local clone of the repository from <code>kaufmann</code> on a
personal machine. The key additions / changes are performed here, and
are then pushed to the master repository so that they're visible to the
rest of the team. When preparing to make a new keyring active the
changes that have been sent to the HKP interface are copied from
kaufmann via <code>scp</code> and folded in using the
<code>pull-updates</code> script. The tree is assembled into keyrings
with a simple <code>make</code> and some sanity tests performed using
<code>make test</code>. If these are successful the
<code>sha512sums.txt</code> file is signed using <code>gpg
--clearsign</code> and the output copied over to kaufmann.
<code>update-keyrings</code> is then called to update the active
keyrings (both rsync + HKP). A <code>git push public</code> pushes the
changes to the public repository on <code>salsa</code>. Finally <code>gbp buildpackage --builder='sbuild -d sid'</code> tells-buildpackage to use <code>sbuild</code> to build a package ready to be uploaded to the archive.</p>

<p>(This is a modified version of an article that originally appeared on <a href="https://www.earth.li/~noodles/blog/2017/07/making-a-keyring.html">Jonathan's blog</a>.)</p>

</div>

	<div id="footer">
		<div id="pageinfo">
                  <p>Back to the <a href="https://www.debian.org/">Debian Project homepage</a>.</p>
                  <hr/>
                </div>
<div id="footermap">
<!--UdmComment-->
<p><strong><a href="https://www.debian.org">Home</a></strong></p>
    <ul id="footermap-cola">
		<li><a href="https://www.debian.org/intro/about">About</a>
		  <ul>

		  <li><a href="https://www.debian.org/social_contract">Social&nbsp;Contract</a></li>
		  <li><a href="https://www.debian.org/intro/free">Free&nbsp;Software</a></li>
		  <li><a href="https://www.debian.org/partners/">Partners</a></li>
		  <li><a href="https://www.debian.org/donations">Donations</a></li>
		  <li><a href="https://www.debian.org/contact">Contact&nbsp;Us</a></li>

		  </ul>
		</li>
	</ul>
	<ul id="footermap-colb">
			<li><a href="https://www.debian.org/distrib/">Getting Debian</a>
			  <ul>
			  <li><a href="https://www.debian.org/CD/vendors/">CD vendors</a></li>

			  <li><a href="https://www.debian.org/CD/">CD ISO images</a></li>

			  <li><a href="https://www.debian.org/distrib/netinst">Network install</a></li>
			  <li><a href="https://www.debian.org/distrib/pre-installed">Pre-installed</a></li>
			  </ul>
			</li>
    <li><a href="https://www.debian.org/distrib/packages">Debian&nbsp;Packages</a></li>

	</ul>
	<ul id="footermap-colc">

		<li><a href="https://www.debian.org/News/">News</a>
		  <ul>
		  <li><a href="https://www.debian.org/News/project/">Project&nbsp;News</a></li>
		  <li><a href="https://www.debian.org/events/">Events</a></li>

		  </ul>
		</li>
    <li><a href="https://www.debian.org/doc/">Documentation</a>

      <ul>
      <li><a href="https://www.debian.org/releases/">Release&nbsp;Info</a></li>
      <li><a href="https://www.debian.org/releases/stable/installmanual">Installation&nbsp;manual</a></li>

      <li><a href="https://www.debian.org/doc/books">Debian&nbsp;Books</a></li>
      </ul>
    </li>

   </ul>
   <ul id="footermap-cold">
    <li><a href="https://www.debian.org/support">Support</a>
	  <ul>

			  <li><a href="https://www.debian.org/international/">Debian&nbsp;International</a></li>
			  <li><a href="https://www.debian.org/security/">Security&nbsp;Information</a></li>

			  <li><a href="https://www.debian.org/Bugs/">Bug reports</a></li>
			  <li><a href="https://www.debian.org/MailingLists/">Mailing&nbsp;Lists</a></li>
			  <li><a href="https://lists.debian.org/">Mailing&nbsp;List&nbsp;Archives</a></li>

			  <li><a href="https://www.debian.org/ports/">Ports/Architectures</a></li>
      </ul>

    </li>
</ul>
<ul id="footermap-cole">
    <li><a href="https://www.debian.org/misc/">Miscellaneous</a></li>
    <li><a href="https://www.debian.org/intro/help">Help Debian</a></li>
    <li><a href="https://www.debian.org/devel/">Developers'&nbsp;Corner</a></li>

    <li><a href="https://www.debian.org/sitemap">Site map</a></li>

    <li><a href="https://search.debian.org/">Search</a></li>
</ul>
<!--/UdmComment-->
</div> <!-- end footermap -->
<div id="fineprint">
  <p>To report a problem with information provided in this specific
  web page
  e-mail <a href="mailto:keyring-maint@debian.org">keyring-maint@debian.org</a>. Source
  code for this page is in
  a <a href="https://salsa.debian.org/debian-keyring/website/">git
  repository</a>. For other contact information, see the
  Debian <a href="https://www.debian.org/contact">contact
  page</a>.</p>

  <p>Last Modified: <span class="date">2017-08-29</span>   <br />

  Copyright &copy; 1997-2017
 <a href="http://www.spi-inc.org/">SPI</a>; See <a href="https://www.debian.org/license" rel="copyright">license terms</a><br />
  Debian is a registered <a href="https://www.debian.org/trademark">trademark</a> of Software in the Public Interest, Inc.

</p>
</div>
</div> <!-- end footer -->

  </body>
</html>
